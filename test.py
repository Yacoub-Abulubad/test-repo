#%%
import numpy as np 
import nibabel as nib
import matplotlib.pyplot as plt 
import tensorflow as tf
import scipy
from scipy import ndimage
#%%
def loader(path):
    '''Load the image'''
    raw_img = nib.load(path)
    img = raw_img.get_fdata()
    return img

def normalizer(img):
    '''Normalizing the image'''
    min = 500
    max = 1300
    img[img < min] = min
    img[img > max] = max
    
    img = (img - min) / (max - min)

    return img

def resizer(img, desired_lwd=(128,128,64)):
    '''resize the 3D image'''
    des_len, des_wid, des_dep = desired_lwd
    len, wid, dep = img.shape

    len_factor = len/des_len
    wid_factor = wid/des_wid
    dep_factor = dep/des_dep
    
    len_factor = 1/len_factor
    wid_factor = 1/wid_factor
    dep_factor = 1/dep_factor

    img = ndimage.rotate(img, 90, reshape=False)

    img = ndimage.zoom(img, (len_factor, wid_factor, dep_factor), order=1)

    return img

def image_process(path):
    '''load, process and resize'''
    img = loader(path)
    img = normalizer(img)
    img = resizer(img)

    return img

    
    


# %%

# %%
